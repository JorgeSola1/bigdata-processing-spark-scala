# Practice of Big Data processes

## Keepcoding project

This project is an example of how to read data in streaming and batch using the framework od spark.
In this case, there are three fields:

 - Streaming
 - Batch
 - Provisioner

### Streaming
We use a Virtual Machine to run a docker container which we'll use to send streaming data. We'll recover all the data, and we'll save them in Postgres Database and in a google cloud Storage with ".parquet" format.

We have several methods. Each method has a one function.

 - Read Streaming Data.
 - Change the data structure.
 - Write the data in .parquet format.
 - Save the data in a Postgres database.
 
The most interesting is that spark let us change the format, and introduce logical changes in the structure of the data, or add some new fields and make logical operations to create new calculate fields. 

### Provisioner

This part is just to connect with our database and create the neccessary tables to start all the process.

There are four tables:

 - Bytes.
 - Bytes hourly.
 - User quota limit.
 - User metadata.

We'll use the table user_metadata to make a join with the streaming data and calculate some values.

### Batch

This process consist of: 

 - Read the data saved it in the Google Cloud Storage.
 - Calculate the number of bytes group by users, antenna_id and app.
 - Save the process data in postgres.
 
So, we need read the user_metadata a make a join with the streaming data using the common field called "id".
There are several methods to calculate the number of bytes group by user, app or antenna signal.

## How to run the script

 1. You have to introduce the IP address of your computer or your virtual machine. It depends where are you running the docker container which send the streaming data.
 2. You have to create a BBDD and introduce the url, user, password and table name to connect it.
 3. You have to create an Storage and introduce the "url" or "path" to connect it and save the .parquet fields.
4. You have to configurate your computer or virtual machine, to run kafka and send the streaming data.
 