package io.practice.keepcoding.spark.batch

import java.time.OffsetDateTime

import org.apache.spark.sql.functions.{window, _}
import org.apache.spark.sql.types.{DoubleType, StringType}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object AntennaBatchJob extends BatchJob {

  override val spark: SparkSession = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL KeepCoding Base")
    .getOrCreate()

  import spark.implicits._

  override def readFromStorage(storagePath: String, filterDate: OffsetDateTime): DataFrame = {
    spark
      .read
      .format("parquet")
      .load(s"${storagePath}/data")
      .filter(
        $"year" === filterDate.getYear &&
          $"month" === filterDate.getMonthValue &&
          $"day" === filterDate.getDayOfMonth &&
          $"hour" === filterDate.getHour
      )
  }


  override def readUserMetadata(jdbcURI: String, jdbcTable: String, user: String, password: String): DataFrame = {
    spark
      .read
      .format("jdbc")
      .option("url", jdbcURI)
      .option("dbtable", jdbcTable)
      .option("user", user)
      .option("password", password)
      .load()
  }

  override def enrichDevicesWithMetadata(devicesDF: DataFrame, metadataDF: DataFrame): DataFrame = {
    devicesDF.as("device")
      .join(
        metadataDF.as("metadata"),
        $"device.id" === $"metadata.id"
      ).drop($"metadata.id")
  }

  override def computeDevicesCountByAntenna(dataFrame: DataFrame): DataFrame = {

    dataFrame
      .select($"timestamp", $"antenna_id",$"bytes")
      .groupBy($"antenna_id", window($"timestamp", "1 hour"))
      .agg(
        sum($"bytes").as("value")
      )
      .withColumn("type", lit("antenna_bytes_total"))
      .select($"window.start".as("timestamp"), $"antenna_id".cast(StringType).as("id"),$"value",$"type")
  }

  override def computeDevicesCountByUser(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .select($"timestamp", $"id",$"bytes")
      .groupBy($"id", window($"timestamp", "1 hour"))
      .agg(
        sum($"bytes").as("value")
      )
      .withColumn("type", lit("user_bytes_total"))
      .select($"window.start".as("timestamp"), $"id".cast(StringType),$"value",$"Type")

  }

  override def computeDevicesCountByApp(dataFrame: DataFrame): DataFrame = {
    dataFrame
      .select($"timestamp", $"app",$"bytes")
      .withWatermark("timestamp", "1 minute")
      .groupBy($"app", window($"timestamp", "1 hour"))
      .agg(
        sum($"bytes").as("value")
      )
      .withColumn("type", lit("app_bytes_total"))
      .select($"window.start".as("timestamp"), $"app".cast(StringType).as("id"),$"value",$"type")

  }

  override def computeLimitQuoteByUser(dataFrame: DataFrame): DataFrame = {

    dataFrame
      .select($"email", $"bytes", $"quota", $"timestamp")
      .groupBy($"email", window($"timestamp", "1 hour"))
      .agg(
        sum($"bytes").as("usage")
      )
      .filter(
        $"usage" > $"quota"
      )
      .select($"email", $"usage",$"quota", $"window.start".as("timestamp"))
  }

  override def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Unit = {
    dataFrame
      .write
      .mode(SaveMode.Append)
      .format("jdbc")
      .option("driver", "org.postgresql.Driver")
      .option("url", jdbcURI)
      .option("dbtable", jdbcTable)
      .option("user", user)
      .option("password", password)
      .save()
  }

  override def writeToStorage(dataFrame: DataFrame, storageRootPath: String): Unit = {
    dataFrame
      .write
      .partitionBy("year", "month", "day", "hour")
      .format("parquet")
      .mode(SaveMode.Overwrite)
      .save(s"${storageRootPath}/historical")
  }

  def main(args: Array[String]): Unit = run(args)
}
