package io.practice.keepcoding.spark.batch

import java.sql.Timestamp
import java.time.OffsetDateTime
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

import org.apache.spark.sql.{DataFrame, SparkSession}

case class DevicesMessage(bytes:Long,timestamp: Timestamp, app: String, id: String, antenna_id: String)

trait BatchJob {

  val spark: SparkSession

  def readFromStorage(storagePath: String, filterDate: OffsetDateTime): DataFrame

  def readUserMetadata(jdbcURI: String, jdbcTable: String, user: String, password: String): DataFrame

  def enrichDevicesWithMetadata(antennaDF: DataFrame, metadataDF: DataFrame): DataFrame

  def computeDevicesCountByAntenna(dataFrame: DataFrame): DataFrame

  def computeDevicesCountByUser(dataFrame: DataFrame): DataFrame

  def computeDevicesCountByApp(dataFrame: DataFrame): DataFrame

  def computeLimitQuoteByUser(dataFrame: DataFrame): DataFrame

  def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Unit

  def writeToStorage(dataFrame: DataFrame, storageRootPath: String): Unit

  def run(args: Array[String]): Unit = {
    val Array(filterDate, storagePath, jdbcUri, jdbcMetadataTable, aggJdbcTable, aggJdbcLimitQuote, jdbcUser, jdbcPassword) = args
    println(s"Running with: ${args.toSeq}")

    val devicesDF = readFromStorage(storagePath, OffsetDateTime.parse(filterDate))
    val metadataDF = readUserMetadata(jdbcUri, jdbcMetadataTable, jdbcUser, jdbcPassword)
    val devicesMetadataDF = enrichDevicesWithMetadata(devicesDF, metadataDF).cache()
    val aggByAntennaDF = computeDevicesCountByAntenna(devicesDF)
    val aggByUserDF = computeDevicesCountByUser(devicesDF)
    val aggByAppDF = computeDevicesCountByAntenna(devicesDF)
    val userQuoteLimit = computeLimitQuoteByUser(devicesMetadataDF)

    //val aggPercentStatusDF = computePercentStatusByID(devicesMetadataDF)
    //val aggErroAntennaDF = computeErrorAntennaByModelAndVersion(devicesMetadataDF)

    writeToJdbc(aggByAntennaDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    writeToJdbc(aggByUserDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    writeToJdbc(aggByAppDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    writeToJdbc(userQuoteLimit, jdbcUri, aggJdbcLimitQuote, jdbcUser, jdbcPassword)

    devicesDF
      .writeStream
      .format("console")
      .start()
      .awaitTermination()

    writeToStorage(devicesDF, storagePath)

    spark.close()
  }

}
