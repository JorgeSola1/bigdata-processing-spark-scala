package io.practice.keepcoding.spark.streaming

import java.sql.Timestamp
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

import org.apache.spark.sql.{DataFrame, SparkSession}

case class DevicesMessage(bytes:Long,timestamp: Timestamp, app: String, id: String, antenna_id: String)

trait StreamingJob {

  val spark: SparkSession

  def readFromKafka(kafkaServer: String, topic: String): DataFrame

  def parserJsonData(dataFrame: DataFrame): DataFrame

  def bytesGroupByUser (dataFrame: DataFrame): DataFrame

  def bytesGroupByApp (dataFrame: DataFrame): DataFrame

  def bytesGroupByAntenna (dataFrame: DataFrame): DataFrame

  def writeToJdbc(dataFrame: DataFrame, jdbcURI: String, jdbcTable: String, user: String, password: String): Future[Unit]

  def writeToStorage(dataFrame: DataFrame, storageRootPath: String): Future[Unit]

  def run(args: Array[String]): Unit = {
    val Array(kafkaServer, topic, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword, storagePath) = args
    //val Array(kafkaServer, topic) = args
    println(s"Running with: ${args.toSeq}")
    println(args)

    val kafkaDF = readFromKafka(kafkaServer, topic)
    val devicesDF = parserJsonData(kafkaDF)

    val storageFuture = writeToStorage(devicesDF, storagePath)
    //val aggByUserDF = bytesGroupByUser(devicesDF)
    //val aggByAppDF = bytesGroupByApp(devicesDF)
    //val aggByAntennaDF = bytesGroupByAntenna(devicesDF)
    //val aggFutureUser = writeToJdbc(aggByUserDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    //val aggFutureApp = writeToJdbc(aggByAppDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)
    //val aggFutureAntenna = writeToJdbc(aggByAntennaDF, jdbcUri, aggJdbcTable, jdbcUser, jdbcPassword)

    //Await.result(Future.sequence(Seq(storageFuture, aggFutureAntenna, aggFutureUser, aggFutureApp)), Duration.Inf)
    Await.result(Future.sequence(Seq(storageFuture)), Duration.Inf)

    spark.close()
  }

}
